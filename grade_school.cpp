#include "grade_school.h"
#include <iostream>
#include <algorithm>
using namespace std;

namespace grade_school {
  void school::add(string name, int grade) {
    /* Add a name into the grade's list.
     * Don't check for duplicates
     */

    // grade exists and we need to insert new name into it
    SchoolList &old_list = roster_[grade];
    auto it = lower_bound(old_list.begin(), old_list.end(), name);
    old_list.insert(it, move(name));
  }

  Roster school::roster() {
    /* Get the roster
     */
    return roster_;
  }

  SchoolList school::grade(int grade_number) {
    /* Get the grade's list
     */
    // roster contains grade
    try {
      return roster_.at(grade_number);
    } catch (const out_of_range& oor) {
      return SchoolList();
    }
  }
}
