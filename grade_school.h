#ifndef GRADE_SCHOOL_GRADE_SCHOOL_H_
#define GRADE_SCHOOL_GRADE_SCHOOL_H_

#include <vector>
#include <string>
#include <map>

typedef std::vector<std::string> SchoolList;
typedef std::map<int, SchoolList> Roster;

namespace grade_school {
  class school {
  private:
    Roster roster_;
  public:
    Roster roster();
    void add(std::string name, int grade);
    SchoolList grade(int g);
  };
}

#endif // GRADE_SCHOOL_GRADE_SCHOOL_H_
